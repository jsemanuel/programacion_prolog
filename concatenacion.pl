%append([],L,L).
%append([X|L1],L2,[X,L3]).
piezabasica(llanta).
piezabasica(radio).
piezabasica(cuadrotrasero).
piezabasica(manillar).
piezabasica(pinones).
piezabasica(tornillo).
piezabasica(tuerca).
piezabasica(horquilla).

ensamblaje(bici,[rueda,rueda,cuadro]).
ensamblaje(rueda,[radio,llanta,plato]).
ensamblaje(cuadro,[cuadrotrasero,cuadrodelantero]).
ensamblaje(cuadrodelantero,[horquilla,manillar]).
ensamblaje(plato,[pinones,eje]).
ensamblaje(eje,[tornillo,tuerca]).
%el programa listarar las piezas basicas utilizando append
% piezasde(X,Y) Xes la pieza e Y es la lista de las piezas basicas que
% conforma X. si x es un apieza basica el programa devuelve X
piezasde(X,[X]):-
	piezabasica(X).
piezasde(X,P):-
	ensamblaje(X,Subpiezas),
	listaspiezasde(Subpiezas,P).
listaspiezasde([],[]).
listaspiezasde([P|Cola],Total):-
		piezasde(P,Piezascabeza),
		listaspiezasde(Cola,Piezascola),
		append(Piezascabeza,Piezascola,Total).








