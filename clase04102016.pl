%var(X). true
%var(30). false
%nonvar(X). false
%nonvar(30). true
%atom('inf-282'). true
%integer(X). es true si X continene un entero
%atomic(X):-atom(X.) es verdad si x es un atomo
%atomic(X):-integer(X).es verdad si x es un entero
%listing(X).
%clause(X,Y). Se satisafce
%asserta(X). a�ade una clausula al principio de la lista
%assertz(X). a�ade una clausula al final de la lista
%retract(X). la unica forma de eliminar entonces la clausula
%si se satisafce elimina la primera clausula
%si se reevalua elimina las siguientes
%functor(T,F,N).
%T es una estructura con functor F y aridad N
% el predicado se usa de dos maneras Si T no esta insatnciada, deben
% estarlo F y N . T devolvera las estructuras q verifiquen F y N.
% arg(N,T,A) el argumento N de T es A
% 22 ?- arg(2,relacionado(juan,madre(juana)),X).
%X = madre(juana).
%23 ?- arg(1,a+(b+c),X).
%X = a.
%24 ?- arg(2,[a,b,c],X).
%X = [b, c].
%X=..L (univ)
%26 ?- [a,b,c,d]=..L.
%L = ['[|]', a, [b, c, d]].
%27 ?- (a+b)=..L.
%L = [+, a, b].
%28 ?- (a+b)=..[+,X,Y].
%X = a,
%Y = b.

s(+,X,Y,Z):-
	integer(X),
	integer(Y),
	Z is X+Y.
copiar(Viejo,Nuevo):-
	functor(Viejo,F,N),
	functor(Nuevo,F,N).


